# SOLUCION EJERCICIOS DE LA SESION 4

Hacer peticiones al servidor de Mongo desde Mongo Compass & Mongo Shell para hacer los siguientes ejercicios:

1. Listar todos los contactos.
> db.Contacts.find()

2. Busca el primer contacto que sea de Alemania (Germany).
> db.Contacts.find({"location.country": "Germany"}).limit(1) o > db.contactos.findOne({"location.country": "Germany"})

3. Busca todos los contactos que tengan Blake como nombre (first).
> db.Contacts.find({"name.first": "Blake"})

4. Busca los primeros 5 contactos que tengan como género (gender) hombre (male)
> db.Contacts.find({gender: "male"}).limit(5)

5. Devuelve los 4 primeros contactos ordenados por nombre (name) de manera descendente.
> db.Contacts.find().sort({"name.title": 1, "name.first": 1, "name.last": 1}).limit(4)

6. Clona la colección de Contacts a CopiaContacts y luego bórrala.
> db.Contacts.aggregate({$out: "CopiaContacts"})
> db.Contacts.drop()

7. Renombra el campo de name por nombre.
> db.CopiaContacts.updateMany({}, {$rename: {name: "nombre"}})

8. Borra todos los contactos que tengan como estado (state) Florida.
> db.CopiaContacts.deleteMany({"location.state": "Florida"})

9. Muestra las primeras 5 ciudades que empiecen por A ordenadas de manera ascendente, las soluciones deben ser únicas.
> db.CopiaContacts.aggregate({$unwind: "$location.city"}, {$match: {"location.city": {$regex: /^A/}}}, {$sort: {"location.city": 1}}, {$limit: 5})

10. Crea una colección a parte, que solo contenga a los contactos de Francia (France) y que tengan entre 18 y 50 años. Usa una agregación para ello.
> db.CopiaContacts.aggregate({$match: {"location.country": "France", "dob.age": {$gt: 18, $lt: 50}}}, {$out: "Francia"})

11. Añade un número favorito a cada contacto, luego crea un bucket agrupando por número favorito que separe en 5 segmentos.
> db.CopiaContacts.aggregate({$addFields: {favoriteNumber: {$floor: {$divide: [{$subtract: [{$add: ["$dob.age", 1]}, 18]}, 5]}}}})

12. En la colección de Contatcs, haz una proyección la cual tiene que devolver solo el name y username del contacto.
> db.CopiaContacts.aggregate({$project: {"nombre.first": 1, "login.username": 1}})

13. Haz una consulta en la colección de Contacts la cual devuelva un documento por cada nombre (name) y que sea único, ordenado por apellido (last), tienes que usar el operador $unwind.
> db.CopiaContacts.aggregate({$unwind: "$nombre"}, {$group: {_id: "$nombre.last", nombre: {$first: "$nombre.first"}}}, {$sort: {_id: 1}})

14. Haz una proyección convirtiendo la fecha (date) a un formato DD-MM-AAAA, la nueva variable será fechaNacimiento
> db.CopiaContacts.aggregate({$project: {"fechaNacimiento": {$dateToString: {format: "%d-%m-%Y", date: "$dob.date"}}}})