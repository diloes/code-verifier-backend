import express, { Express, Request, Response } from 'express'

// Swagger
import swaggerUi from 'swagger-ui-express'

// Security
import cors from 'cors'
import helmet from 'helmet'

// TODO: HTTPS

// Root Router
import rootRouter from '../routes' // por defecto, importa el index.ts
import mongoose from 'mongoose'

// Create Express App
const server: Express = express()

// Swagger Config and route
server.use(
  '/docs',
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: '/swagger.json',
      explorer: true
    }
  }))

// Define SERVER to use '/api' and use rootRouter from 'index.ts' in routes
server.use('/api', rootRouter) // http://localhost:8000/api/...

// Static Server
server.use(express.static('public'))

// Mongoose Connection to our database 'codeverification'
mongoose.connect('mongodb://localhost:27017/codeverification')

// Security config
server.use(helmet())
server.use(cors())

// Content Type Config:
server.use(express.urlencoded({ extended: true, limit: '50mb' }))
server.use(express.json({ limit: '50mb' })) // requests tipo json con limite de 50mb

// http://localhost:8000/ -> http://localhost:8000/api
server.get('/', (req: Request, res: Response) => {
  res.redirect('/api')
})

export default server
