
// Basic JSON response for Controllers
export type BasicResponse = {
  message: string
}

// Error JSON response for controllers
export type ErrorResponse = {
  error: string,
  message: string
} 

// Date JSON response for controllers
export type DateResponse = {
  message: string,
  Date: Date 
}

// Auth JSON response for controllers
export type AuthResponse = {
  message: string,
  token: string
}