import { Get, Query, Route, Tags } from 'tsoa'
import { BasicResponse } from './types'
import { IHelloController } from './interfaces'
import { LogSuccess } from '../utils/logger'

@Route('/api/hello')
@Tags('HelloController')
export class HelloController implements IHelloController {
  /**
   * Endpoint to retreive a Message 'Hello {name}' in JSON
   * @param { string | undefined } name Name of user to be greeted
   * @returns { BasicResponse } Promise of BasicResponse
   */
  @Get('/')
  public async getMessage (@Query()name?: string): Promise<BasicResponse> {
    // LogSucces recibe un string y hace un console.log
    LogSuccess('[/api/hello] Get Request')

    // retornamos una promesa de tipo BasicResponse
    return {
      message: `Hello ${name || 'anónimo'}`
    }
  }
}

/*
NOTAS:
 - La clase HelloController se va a encargar de gestionar determinadas rutas
 a través de las rutas identificaremos cual es el controller o que función
 tiene que ejecutar el contoler.
 - Recordamos que BasicResponse un tipo complejo personaliado ->
 tipo complejo = Promesa + personalizado = BasicResponse
*/
