import { Delete, Get, Post, Put, Query, Route, Tags } from 'tsoa'
import { IKataController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'
import { Ikata } from '../domain/interfaces/IKata.interface'

// ORM - Katas Collection
import { getKatas, getKataById, createKata, deleteKataById, updateKataById } from '../domain/orm/Katas.orm'


export class KatasController implements IKataController {
  
  /**
   * Endpoint to retrieve the katas in the collection 'katas' of db
   * @param page 
   * @param limit 
   * @param {string} id
   * @returns All katas or a kata by id 
   */
  @Get('/')
  public async getKatas(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
    
    let response: any = ''

    if(id){
      LogSuccess(`[/api/katas] - Get Kata By Id: ${id}`)
      response = await getKataById(id)
    }else {
      LogSuccess(`[/api/katas] - Get All Katas`)
      response = await getKatas(page, limit)
    }

    return response
  }

  @Post('/')
  public async createKata(kata: Ikata): Promise<any> {
    
    let response: any = ''

    if(kata){
      LogSuccess(`[/api/katas] - Create Kata`)
      await createKata(kata).then( r => {
        LogSuccess(`[/api/katas] - Kata created successfully: ${kata.name}`)
        response = {
          message: `Kata with id ${kata.name} created successfully`,
        }
      })
    }else {
      LogWarning(`[/api/katas] - Need to pass a kata entity`)
      response = {
        message: `Kata not registered. Please, provide a kata entity`,
      }
    }

    return response
  }

  /**
   * Endpoint to delete katas in the collection 'katas' of db
   * @param {string} id Id of kata to delete
   * @returns message informing if deletion was successful
   */
  @Delete('/')
  public async deleteKata(@Query()id: string): Promise<any> {
    
    let response: any = ''

    if(id){
      LogSuccess(`[/api/katas] - Delete Kata By Id: ${id}`)
      await deleteKataById(id).then( r => {
        response = {
          message: `Kata with id ${id} deleted successfully`,
        }
      })
    }else {
      LogWarning(`[/api/katas] - Delete Kata By Id: ${id}`)
      response = {
        message: `Kata with id ${id} not found`,
      }
    }

    return response
  }

  @Put('/')
  public async updateKataById(id: string, kata: any): Promise<any> {
    
    let response: any = ''

    if(id){
      LogSuccess(`[/api/katas] - Update Kata By Id: ${id}`)
      await updateKataById(id, kata).then( r => {
        response = {
          message: `Kata with id ${id} updated successfully`,
        }
      })
    }else {
      LogWarning(`[/api/katas] - Update Kata By Id: ${id}`)
      response = {
        message: `Kata with id ${id} not found`,
      }
    }
    
    return response
  }

}
  
  /**
  * Endpoint to retrieve the Kata by level OR last 5 katas
  * @param {number} level Level of kata to retreive
  * @param {boolean} recent If true, return the last 5 katas
  * @returns Kata found by level OR last 5 katas
  
  @Get('/')
  public async getKatasLevel(@Query()page: number, @Query()limit: number, @Query()level?: number, @Query()recent?: boolean): Promise<any> {

    let response: any = ''

    if(level && !recent) { 
      LogSuccess(`[/api/katas] Get kata by level: ${level}`)
      response = await getKatasByLevel(page,limit,level)
    }else if (!level && recent) { 
      LogSuccess(`[/api/katas] Get katas recents`)
      response = await getKatasRecent()
    }else { 
      LogWarning('[/api/users] - ERROR - No level provided')
      response = {
        message: 'Please, provide an level'
      }

      return response
    } 
  }

  
   * Endpoint to valorate a kata
   * @param {string} id ID of kata to valorate
   * @param {number} score Score of kata to valorate
   * @returns Kata valorated
   
  @Put('/')
  public async valorateKata(@Query()id: string, @Query()score: number): Promise<any> {

    let response: any = ''

    LogSuccess(`[/api/katas] Valorating kata: ${id}`)
    response = await valorateKata(id, score)
  }
}
*/
 