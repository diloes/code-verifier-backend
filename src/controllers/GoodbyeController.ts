import { Get, Query, Route, Tags } from 'tsoa'
import { DateResponse } from './types'
import { IHelloController } from './interfaces'
import { LogSuccess } from '../utils/logger'

@Route('/api/goodbye')
@Tags('GoodbyeController')
export class GoodbyeController implements IHelloController {
  /**
   * Endpoint to retreive a Message 'Goodbye {name}' and date in JSON
   * @param { string | undefined } name Name of user
   * @returns { DateResponse } Promise of BasicResponse
   */
  @Get('/')
  public async getMessage (@Query()name?: string): Promise<DateResponse> {
    LogSuccess('[/api/goodbye] Get Request')

    return {
      message: `Goodbye ${name || 'anónimo'}`,
      Date: new Date()
    }
  }
}
