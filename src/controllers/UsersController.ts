import { Delete, Get, Post, Put, Query, Route, Tags } from 'tsoa'
import { IUsersController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'

// ORM - Users Collection
import { getAllUsers, getUserByID, deleteUserByID, updateUserByID, getKatasFromUser } from '../domain/orm/User.orm'

// Body Parser to read body from request
import bodyParser from 'body-parser'

const jsonParser = bodyParser.json()


@Route('/api/users')
@Tags('UsersController')
export class UsersController implements IUsersController {
  
  /**
   * Endpoint to retrieve the Users in the collection 'Users' of DB
   * @param {string} id ID of user to retreive (optional)
   * @returns All users o user found by ID
   */
  @Get('/')
  public async getUsers(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {

    let response: any = ''

    if(id){ // Si nos llega el id como argumento
      LogSuccess(`[/api/users] Get user by Id: ${id}`)

      // De manera asíncrona obtenemos el user por id
      response = await getUserByID(id)

    }else { // Si no tenemos id como argumento en la llamada a la función
      LogSuccess('[/api/users] - GET - Get all Users')
      // De manera asincrona obtenemos todos los usuarios
      response = await getAllUsers(page, limit)
    }

    return response


  }

  /**
   * Endpoint to delete the Users in the collection 'Users' of DB
   * @param {string} id ID of user to delete (optional)
   * @returns message informing if deletion was correct
   */
  @Delete('/')
  public async deleteUser(@Query()id?: string): Promise<any> {

    let response: any = ''

    if(id){ // Si nos llega el id como argumento

      LogSuccess(`[/api/users] DELETE user by Id: ${id}`)

      // De manera asíncrona obtenemos el user por id
      await deleteUserByID(id).then( r => {
        response = {
          status: 204,
          message: `User with id ${id} deleted successfully`
        }
      })

    }else { // Si no tenemos id como argumento en la llamada a la función

      LogWarning('[/api/users] - DELETE - Delete user request WITHOUT ID')
      response = {
        status: 400,
        message: 'Please, provide an ID to remove from database'
      }

    }

    return response
  }

  @Put('/')
  public async updateUser(@Query()id: string, user: any): Promise<any> {

    let response: any = ''

    if(id){ // Si nos llega el id como argumento

      LogSuccess(`[/api/users] UPDATE user by Id: ${id}`)
      await updateUserByID(id, user).then( r => {
        response = {
          message: `User with id ${id} updated successfully`
        }
      })

    }else { // Si no tenemos id como argumento en la llamada a la función

      LogWarning('[/api/users] - UPDATE - Delete user request WITHOUT ID')
      response = {
        message: 'Please, provide an ID to update an existing user'
      }

    }

    return response
  }

  @Get('/katas')
  public async getKatas(@Query()page: number, @Query()limit: number, @Query()id: string): Promise<any> {
    
    let response: any = ''

    if(id){ // Si nos llega el id como argumento
      LogSuccess(`[/api/users/katas] Get katas from user by Id: ${id}`)
      response = await getKatasFromUser(page, limit, id)
    }else {
      LogWarning('[/api/users/katas] - GET - Get katas request WITHOUT ID')
      response = {
        message: 'Please, provide an ID to get katas from user'
      }
    }
    return response
  }
}
  


