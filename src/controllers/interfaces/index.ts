import { IUser } from '../../domain/interfaces/IUser.interface'
import { BasicResponse } from '../types'

// Cuando implementemos este controlador...
export interface IHelloController {

  /* ...Tiene una función implementada getMessage que va a recibir un name opcional
  de tipo string y va a devolver una promesa de tipo BasicResponse */
  getMessage(name?:string): Promise<BasicResponse>
}

export interface IUsersController {
  
  // Read all users from database || get User by ID
  getUsers(page: number, limit: number, id?: string): Promise<any>

  // Delete user by ID
  deleteUser(id?: string): Promise<any>

  // Update user
  updateUser(id: string, user: any): Promise<any>

  // Get katas  of User
  getKatas(page: number, limit: number, id?: string): Promise<any>

}

/* export interface IKatasController {

  // Get Kata by level
  getKatasLevel(page: number, limit: number, level?: number): Promise<any>

  // Valorate Kata
  valorateKata(id: string, score: number): Promise<any>


} */

export interface IAuthController {
  
  // Register new user
  registerUser(user: IUser): Promise<any>

  // Login user
  loginUser(auth: any): Promise<any>

}

export interface IKataController {

  // Read all katas
  getKatas(page: number, limit: number, id?: string): Promise<any>

  // Create new kata
  createKata(kata: any): Promise<any>

  // Delete kata by id
  deleteKata(id: string): Promise<any>

  // Update kata by id
  updateKataById(id: string, kata: any): Promise<any>

}

/**
 * NOTAS: 
 * - Cuando en los parametros se añade una signo de interrogación estamos haciendo
 * el parametro opcional. Y se puede llamar a la función con argumentos o no. 
 */