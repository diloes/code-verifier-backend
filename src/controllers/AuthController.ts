import { Delete, Get, Post, Put, Query, Route, Tags } from 'tsoa'
import { IAuthController, IUsersController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'
import { IUser } from '../domain/interfaces/IUser.interface'
import { IAuth } from '../domain/interfaces/IAuth.interface'
import { AuthResponse, ErrorResponse } from './types'

// ORM imports
import { registerUser, loginUser, logoutUser, getUserByID } from '../domain/orm/User.orm'


@Route('/api/users')
@Tags('UsersController')
export class AuthController implements IAuthController {
  
  @Post('/register')
  public async registerUser(user: IUser): Promise<any> {
    
    let response: any = ''

    if(user){
      LogSuccess(`[/api/auth/register] Register New User: ${user.email}`)
      await registerUser(user).then(r => {
        LogSuccess(`[/api/auth/register]: User ${user.email} registered successfully`)
        response = {
          message: `User ${user.name} registered successfully`
        }
      })
    }else {
      LogWarning(`[/api/auth/register]: Register needs User Entity`)
      response = {
        message: 'User not registered. Please, provide a User Entity to register'
      }
    }
    return response
  }

  @Post('/login')
  public async loginUser(auth: IAuth): Promise<any> {

    let response: AuthResponse | ErrorResponse | undefined

    if(auth){
      LogSuccess(`[/api/auth/register] User logged: ${auth.email}`)
      let data = await loginUser(auth)
      response = {
        token: data.token,
        message: `Welcome, ${data.user.name}`
      }
    }else {
      LogWarning(`[/api/auth/login]: Login needs Auth Entity(email, password)`)/*  */
      response = {
        error: '[AUTH ERROR]: Email & Password are needed',
        message: 'Please, provide a email and password to login'
      }
    }
    return response
  }

  @Post('/logout')
  public async logoutUser(): Promise<any> {

    let response: any = ''

    // TODO: Close session
    throw new Error('Method not implemented.')    
  }

  /**
   * Endpoint to retreive the User in the collection 'Users' of DB
   * Middleware: Validate JWT
   * In headers you must add the x-access-token with a valid JWT
   * @param {string} id Id of user to retreive (optional)
   * @returns Data of user found by id 
   */
  @Get('/me')
  public async userData(@Query()id: string) : Promise<any> {

    let response: any = ''

    if(id){
      LogSuccess(`[/api/users] Get user data by ID: ${id}`)
      response = await getUserByID(id)
    }

    return response
  }
}
