export enum KataLevel {
  BASIC = 'Basic',
  MEDIUM = 'Medium',
  HIGH = 'High',
}

export interface Ikata {
  name: string,
  description: string,
  level: KataLevel,
  intents: number,
  stars: number,
  creator: string, // Id of user
  solution: string
  participants: string[],
}

/**
 * NOTAS:  
 * - En los enums si no le damos un valor, por defecto es 0 para el primero, 1 para el segundo 
 * y asi sucesivamente.
 */