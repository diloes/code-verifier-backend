import { kataEntity } from '../entities/Kata.entity'
import { LogError } from '../../utils/logger'
import { Ikata } from '../interfaces/IKata.interface'
import dotenv from 'dotenv'

// Load env vars
dotenv.config()

// Method to obtain all Katas from collection 'katas' in mongo server
export const getKatas = async (page: number, limit: number): Promise<any[] | undefined> => {
  try {
    
    const kataModel = kataEntity()
    let response: any = ''

    // Search all katas
    await kataModel.find({isDeleted: false})
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: Ikata[]) => {
        response.katas = katas
      })
    
    // Count total documents in collection 'katas'
    await kataModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    
    return response

  } catch (error) {
    LogError(`[ORM ERROR] - GetKatas: ${error}`)
  }
}

// Obtain kata by id
export const getKataById = async (id: string): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.findById(id)
  } catch (error) {
    LogError(`[ORM ERROR] - GetKataById: ${error}`)
  }
}

// Delete kata by id
export const deleteKataById = async (id: string): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.deleteOne({_id: id})
  } catch (error) {
    LogError(`[ORM ERROR] - DeleteKataById: ${error}`)
  }
}

// Create new kata
export const createKata = async (kata: Ikata): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.create(kata)
  } catch (error) {
    LogError(`[ORM ERROR] - CreateKata: ${error}`)
  }
} 

// Update kata by id
export const updateKataById = async (id: string, kata: Ikata): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.updateOne({_id: id}, kata)
  } catch (error) {
    LogError(`[ORM ERROR] - UpdateKataById: ${error}`)
  }
}


/* // Filtrar las Katas disponibles por nivel de dificultad
export const getKatasByLevel = async (page: number, limit: number, level: number) : Promise<any[] | undefined> => {
    
  try {
    // Instanciamos la colección con la que vamos a trabajar
    const kataModel = kataEntity()
    
    // Search all katas by level
    return await kataModel.find({ level: level })
    .limit(limit) // limite de users por pagina
    .skip((page - 1) * limit) // pagina actual - 1 * limite de users por pagina = desplazamiento de users 
  
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all katas by level: ${error}`)
  } 
}

// Obtener las 5 Katas más recientes
export const getKatasRecent = async (): Promise<any[] | undefined> => {
      
  try {
    // Instanciamos la colección con la que vamos a trabajar
    const kataModel = kataEntity()
    
    // Retornamos las 5 katas más recientes
    return await kataModel.find({}).sort({ date: -1 }).limit(5)
  
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all katas by level: ${error}`)
  } 
}

// Valorar una Kata
export const valorateKata = async (id: string, score: number): Promise<any> => {
  try {
    // Instanciamos la colección con la que vamos a trabajar
    const kataModel = kataEntity()
    
    // Actualizamos la Kata
    return await kataModel.updateOne({ _id: id }, { $set: { valoration: score } })
  
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all katas by level: ${error}`)
  } 
} */