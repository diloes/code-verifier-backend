import { userEntity } from '../entities/User.entity'
import { kataEntity } from '../entities/Kata.entity'
import { LogError } from '../../utils/logger'
import { IUser } from '../interfaces/IUser.interface'
import { IAuth } from '../interfaces/IAuth.interface'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
import { Ikata } from '../interfaces/IKata.interface'
import mongoose from 'mongoose'


// Configuration of enviroment variables
dotenv.config()

// Obtain secret key to generate JWT
const secret = process.env.SECRETKEY || 'MYSECRETKEY'

// CRUD
// Method to obtain all Users from Collection 'Users' in Mongo Server
export const getAllUsers = async (page: number, limit: number): Promise <any[] | undefined> => {
  
  try {
    const userModel = userEntity()  

    let response: any = {}

    // Search all users (using pagination)
    await userModel.find({isDelete: false})
      .select('name email age katas') // campos que vamos a retornar(todos menos password)
      .limit(limit) // limite de users por pagina
      .skip((page - 1) * limit) // pagina actual - 1 * limite de users por pagina = desplazamiento de users 
      .exec().then((users: IUser[]) => { // ejecuta
        response.users = users
      })
    
    // Count total documents in collection 'users'
    await userModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })

    return response

  } catch (error) {
    LogError(`[ORM ERROR]: Getting all users: ${error}`)
  }
}

// Get user by ID
export const getUserByID = async (id: string) : Promise<any | undefined> => {

  try {
    // Instanciamos la colección con la que vamos a trabajar
    const userModel = userEntity()

    // Search User by ID
    return await userModel.findById(id).select('name email age katas')

  } catch (error) {
    LogError(`[ORM ERROR]: Getting all users: ${error}`)
  } 
}

// Delete user by ID
export const deleteUserByID = async (id: string) : Promise<any | undefined> => {
  
  try {
    // Instanciamos la colección con la que vamos a trabajar
    const userModel = userEntity()

    // Delete User by ID
    return await userModel.deleteOne({ _id: id })

  } catch (error) {
    LogError(`[ORM ERROR]: Deleting user by id: ${error}`)
  }
}

// Update user by ID
export const updateUserByID = async (id: string, user: any, ) : Promise<any | undefined> => {

  try {
    // Instanciamos la colección con la que vamos a trabajar
    const userModel = userEntity()   
    
    // Update user
    return await userModel.findByIdAndUpdate(id, user)
    
  } catch (error) {
    LogError(`[ORM ERROR]: Updating User: ${user} - ${error}`)
  }

}

// Register user
export const registerUser = async (user: IUser) : Promise<any | undefined> => {

  try{
    // Instanciamos la colección con la que vamos a trabajar
    const userModel = userEntity()

    // Creamos el nuevo usuario
    return await userModel.create(user)

  }catch(error){
    LogError(`[ORM ERROR]: Registering User: ${error}`)
  }

}

// Login user
export const loginUser = async (auth: IAuth) : Promise<any | undefined> => {
  
    try{
      // Instanciamos la colección con la que vamos a trabajar
      const userModel = userEntity()

      let userFound: IUser | undefined = undefined
      let token = undefined
      
      // Check if user exists by unique email
      await userModel.findOne({email: auth.email}).then((user: IUser) => {
        userFound = user
      }).catch(error => {
        console.error(`[ERROR Authentication in ORM]: User not found`)
        throw new Error(`[ERROR Authentication in ORM]: User not found: ${error}`);
      })

      // Check if password is valid (compare with bcrypt)
      let validPassword = bcrypt.compareSync(auth.password, userFound!.password)

      if(!validPassword){
        console.error(`[ERROR Authentication in ORM]: Password not valid`)
        throw new Error(`[ERROR Authentication in ORM]: Password not valid`);
      }

      // Generate our JWT
      token = jwt.sign({email: userFound!.email}, secret),{
        expiresIn: "2h"
      }

      return {
        user: userFound,
        token
      }
     
    }catch(error){
      LogError(`[ORM ERROR]: Logging User: ${error}`)
    }
  
}

// Logout user
export const logoutUser = async () : Promise<any | undefined> => {
  // TODO: NOT IMPLEMENTED
}

// Get katas from user
export const getKatasFromUser = async (page: number, limit: number, id:string ): Promise<any[] | undefined> => {
  try {
      let userModel = userEntity();
      let katasModel = kataEntity();

      let katasFound: Ikata[] = [];

      let response: any = {
          katas: []
      };

      console.log('User ID', id);

      await userModel.findById(id).then(async (user: IUser) => {
          response.user = user.email;
          
          // console.log('Katas from User', user.katas);

          // Create types to search
          let objectIds:mongoose.Types.ObjectId[]  = [];
          user.katas.forEach((kataID: string) => {
              let objectID = new mongoose.Types.ObjectId(kataID);
              objectIds.push(objectID);
          });

          await katasModel.find({"_id": {"$in": objectIds }}).then((katas: Ikata[]) => {
              katasFound = katas;
          });

      }).catch((error) => {
          LogError(`[ORM ERROR]: Obtaining User: ${error}`);
      })

      response.katas = katasFound;

      return response;

  } catch (error) {
      LogError(`[ORM ERROR]: Getting All Users: ${error}`);
  }
}



// ORM makes the CRUD operations