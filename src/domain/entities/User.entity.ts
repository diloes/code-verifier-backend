import mongoose from 'mongoose'
import { IUser } from '../interfaces/IUser.interface'

export const userEntity = () => {
  
  const userSchema = new mongoose.Schema<IUser>(
    {
      name: { type: String, required: true },
      email: { type: String, required: true },
      password: { type: String, required: true },
      age: { type: Number, required: true },
      katas: { type: [], required: true }
    }
  )

  // ver Notas
  return mongoose.models.Users || mongoose.model<IUser>('Users', userSchema)
}

/**
 * NOTAS:
 * - En caso de que exista la colección Users, utiliza la misma (return mongoose.models.Users), 
 * si no, la crea (return mongoose.model('Users', userSchema)) donde 'Users' es el nombre de la colección
 * y userSchema es el esquema que se utilizará para crear la colección.
 * - El esquema de mongoose es una clase, por lo que se puede utilizar el método findById().
 */