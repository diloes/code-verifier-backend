import jwt from 'jsonwebtoken'
import { NextFunction, Request, Response } from 'express'
import dotenv from 'dotenv'

// Config dotenv to read enviroment variables
dotenv.config()

const secret = process.env.SECRETKEY || 'MYSECRETKEY'

/**
 * 
 * @param { Request } req Original request previous middleware of verification JWT
 * @param { Response } res Response to verification of JWT
 * @param { NextFunction } next Next function to be executed
 * @returns Errors of verification or next execution
 */
export const verifyToken = (req: Request, res: Response, next: NextFunction) => {

  // Check header from request for 'x-access-token'
  const token: any = req.headers['x-access-token']

  // Verify if jwt is present
  if (!token) {
    return res.status(401).send({
      autentticationError: 'Missing JWT in request header',
      message: 'Not authorized to consume this endopoint'
    })
  }

  // Verify token obtained from request
  jwt.verify(token, secret, (err: any, decoded: any) => {
    
    if(err) {
      return res.status(500).send({
        autentticationError: 'JWT Verification failed',
        message: 'Failed to verify token from request'
      })
    }

    // Pass something to next request ( id of user || other info )    

    // Execute next function -> Protected routes will be executed (¿middleware?)
    next()

  })
}

/**
 * NOTAS:
 * - Esta función es la que se va a encargar de ejecutar o de gestionar las peticiones que están
 * protegidas en nuestro proyecto. 
 */

