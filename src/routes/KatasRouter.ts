import express, { Request, Response } from 'express'
import { KatasController } from '../controllers/KatasController'
import { LogInfo } from '../utils/logger'
import bodyParser from 'body-parser'
import { verifyToken } from '../middlewares/virifyToken.middleware'
import { KataLevel, Ikata } from '../domain/interfaces/IKata.interface'

// Router from express
const katasRouter = express.Router() 

// Body parser middleware
const jsonParser = bodyParser.json()

// Creamos una ruta: ( http://localhost:8000/api/katas )
katasRouter.route('/')
  // * GET:
  .get(verifyToken, async (req: Request, res: Response) => {
    
    // Obtain query params
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10
    const id: any = req?.query?.id
    LogInfo(`Query Params: page: ${page}, limit: ${limit}, id: ${id}`)

    // Call controller
    const controller: KatasController = new KatasController()
    
    // Obtain response
    const response = await controller.getKatas(page, limit, id)

    res.status(200).json(response)
  })

  // * DELETE:
  .delete(verifyToken, async (req: Request, res: Response) => {

    // Obtain query params
    const id: any = req?.query?.id
    LogInfo(`Query Params: id: ${id}`)

    // Call controller
    const controller: KatasController = new KatasController()

    // Obtain response
    const response: any = await controller.deleteKata(id)

    return res.status(200).send(response)
  })

  // * PUT:
  .put(jsonParser, verifyToken, async (req: Request, res: Response) => {

    // Obtain query params
    let id: any = req?.query?.id

    // Read from body
    let name: string = req?.body?.name
    let description: string = req?.body?.description || ''
    let level: KataLevel = req?.body?.level || KataLevel.BASIC
    let intents: number = req?.body?.intents || 0
    let stars: number = req?.body?.stars || 0
    let creator: string = req?.body?.creator
    let solution: string = req?.body?.solution || ''
    let participants: string[] = req?.body?.participants || []

    if(name && description && level && intents >= 0 && stars >= 0 && creator && solution && participants.length >= 0){
      
          // Call controller
    const controller: KatasController = new KatasController()

    const kata: Ikata = {
      name,
      description,
      level,
      intents,
      stars,
      creator,
      solution,
      participants
    }

    // Obtain response
    const response: any = await controller.updateKataById(id, kata)

    // Return response
    return res.status(200).send(response)

    }else {
      return res.status(400).send({
        message: 'Bad Request. Please, provide all the required fields',
      })
    }
  })

  // * POST:
  .post(jsonParser, verifyToken, async (req: Request, res: Response) => {
  
    // Read from body
    let name: string = req?.body?.name
    let description: string = req?.body?.description || 'Default description'
    let level: KataLevel = req?.body?.level || KataLevel.BASIC
    let intents: number = req?.body?.intents || 0
    let stars: number = req?.body?.stars || 0
    let creator: string = req?.body?.creator
    let solution: string = req?.body?.solution || 'Default solution'
    let participants: string[] = req?.body?.participants || []

    if(name && description && level && intents >= 0 && stars >= 0 && creator && solution && participants.length >= 0){
      
          // Call controller
    const controller: KatasController = new KatasController()

    const kata: Ikata = {
      name,
      description,
      level,
      intents,
      stars,
      creator,
      solution,
      participants
    }

    // Obtain response
    const response: any = await controller.createKata(kata)

    // Return response
    return res.status(200).send(response)

    }else {
      return res.status(400).send({
        message: 'Bad Request. Please, provide all the required fields',
      })
    }
    
  })

/*
  // * GET:
  .get(verifyToken ,async (req: Request, res: Response) => {
      
    // Obtain a query params
    let level: any = req?.query?.level
    let recent: any = req?.query?.recent
    LogInfo(`Query Params: ${level}, ${recent}`)

    // Pagination
    let page: any = req?.query?.page || 1
    let limit: any = req?.query?.limit || 10

    // Controller Instance to execute method
    const controller: KatasController = new KatasController()

    // Obtain Response
    const response: any = await controller.getKatasLevel(page, limit, level, recent)

    // Send to the client
    return res.send(response)
  })

  // * PUT:
  .put(verifyToken, async (req: Request, res: Response) => {
    
    // Obtain a query params
    let id: any = req?.query?.id
    let score: any = req?.query?.score
    LogInfo(`Query Params: ${id}, ${score}`)

    // Controller Instance to execute method
    const controller: KatasController = new KatasController()

    // Obtain Response
    const response: any = await controller.valorateKata(id, score)

    // Send to the client
    return res.send(response)

  })
*/
export default katasRouter


