import express, { Request, Response } from 'express'
import { UsersController } from '../controllers/UsersController'
import { LogInfo } from '../utils/logger'
import { IUser } from '../domain/interfaces/IUser.interface'
import { verifyToken } from '../middlewares/virifyToken.middleware'

// Body Parser to read body from request
import bodyParser from 'body-parser'
const jsonParser = bodyParser.json()

// Router from express
const usersRouter = express.Router() // accedemos al sistema de enrutado

// Creamos una ruta: ( http://localhost:8000/api/users?id=134sdfg0934jh/ )
usersRouter.route('/')
  // * GET:
  .get(verifyToken, async (req: Request, res: Response) => {

    // Obtain a query param (ID)
    let id: any = req?.query?.id
    LogInfo(`Query Param: ${id}`)

    // Pagination
    let page: any = req?.query?.page || 1
    let limit: any = req?.query?.limit || 10

    // Controller Instance to execute method
    const controller: UsersController = new UsersController()

    // Obtain Response
    const response: any = await controller.getUsers(page, limit, id)

    // Send to the client 200 (OK)
    return res.status(200).send(response)
  })

  // * DELETE:
  .delete(verifyToken, async (req: Request, res: Response) => {

    //Obtain a query params
    const id: any = req?.query?.id
    LogInfo(`Query param: ${id}`)

    // Controller instance to execute method
    const controller: UsersController = new UsersController()

    // Obtain response
    const response: any = await controller.deleteUser(id)

    return res.status(response.status).send(response.message)
  })

  // * PUT:
  .put(verifyToken, async (req: Request, res: Response) => {

    //Obtain a query params
    const id: any = req?.query?.id
    const name: any = req?.query?.name
    const age: any = req?.query?.age
    const email: any = req?.query?.email
    LogInfo(`Query params: ${id}, ${name}, ${age}, ${email}`)

    // Controller instance to execute method
    const controller: UsersController = new UsersController()

    const user = {
      name: name,
      email: email,
      age: age
    }

    // Obtain response
    const response: any = await controller.updateUser(id, user)

    return res.status(204).send(response)
  })

// Creamos una ruta: ( http://localhost:8000/api/users/katas )
usersRouter.route('/katas')
  .get(verifyToken, async (req: Request, res: Response) => {
      
      // Obtain a query param (ID)
      let id: any = req?.query?.id
      LogInfo(`Query Param: ${id}`)
  
      // Pagination
      let page: any = req?.query?.page || 1
      let limit: any = req?.query?.limit || 10
  
      // Controller Instance to execute method
      const controller: UsersController = new UsersController()
  
      // Obtain Response
      const response: any = await controller.getKatas(page, limit, id)
  
      // Send to the client 200 (OK)
      return res.status(200).send(response)
  })


export default usersRouter

/**
 * 
 * - Get documents => 200 ok
 * - Creation documents => 201 ok
 * - Delete documents => 200 (Entity) | 204 (No return)
 * - Update documents => 200 (Entity) | 204 (No return)
 */