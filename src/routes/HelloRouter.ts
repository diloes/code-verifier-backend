import { BasicResponse } from '@/controllers/types'
import express, { Request, Response } from 'express'
import { HelloController } from '../controllers/HelloController'
import { LogInfo } from '../utils/logger'

// Router from express
const helloRouter = express.Router() // accedemos al sistema de enrutado

// Creamos una ruta: ( http://localhost:8000/api/hello?name=Diego/ )
helloRouter.route('/')
  //* GET:
  .get(async (req: Request, res: Response) => {
    // 1. Obtain a query param
    const name: any = req?.query?.name
    LogInfo(`Query Param: ${name}`)

    // 2. Controller Instance to execute method
    const controller: HelloController = new HelloController()

    // 3. Obtain Response
    const response: BasicResponse = await controller.getMessage(name)

    // 4. Send to the client
    return res.send(response)
  })

export default helloRouter

/*
  NOTAS:
  - helloRouter.route('/') -> Con esto van a estar mucho más controladas las peticiones
  a esta ruta y a todas sus subrutas y métodos que soliciten información de estas.
*/
