import express, { Request, Response } from 'express'
import { GoodbyeController } from '../controllers/GoodbyeController'
import { LogInfo } from '../utils/logger'

// Router from express
const goodbyeRouter = express.Router()

// Creamos la ruta
goodbyeRouter.route('/')
  .get(async (req: Request, res: Response) => {
    // 1. Obtain name from query param
    const name: any = req?.query?.name
    LogInfo(`Query Param: ${name}`)

    // 2. Instanciamos el controlador
    const controller: GoodbyeController = new GoodbyeController()

    // 3. Response
    const response = await controller.getMessage(name)

    // 4. Send to client
    res.send(response)
  })

export default goodbyeRouter
