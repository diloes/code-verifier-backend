import express, { Request, Response } from 'express'
import { AuthController } from '../controllers/AuthController'
import { LogInfo } from '../utils/logger'
import { IUser } from '../domain/interfaces/IUser.interface'
import { IAuth } from '../domain/interfaces/IAuth.interface'

// BCRYPT for password encryption
import bcrypt from 'bcrypt'

// Middleware 
import { verifyToken } from '../middlewares/virifyToken.middleware'

// Body Parser to read JSON from body in request
import bodyParser from 'body-parser'

// Middleware to read JSON in body
const jsonParser = bodyParser.json()

// Router from express
const authRouter = express.Router()

// Creamos una ruta para el registro de usuarios: ( http://localhost:8000/api/auth/register )
authRouter.route('/register')
.post(jsonParser, async (req: Request, res: Response) => {

  let { name, email, password, age } = req?.body
  let hashedPassword: any = ''

  if(name && password && email && age) {

    // Obtain the password in request and cypher it
    hashedPassword = bcrypt.hashSync(password, 8)

    const newUser: IUser = {
      name: name,
      email: email,
      password: hashedPassword,
      age: age,
      katas: []
    }

    // Controller instance to execute method
    const controller: AuthController = new AuthController()

    // Obtain response
    const response: any = await controller.registerUser(newUser)

    return res.status(200).send(response)

  }else {
    return res.status(400).send({
      message: '[ERROR User data missing]: No user can be registered'
    })
  }
})

// Creamos una ruta para el login de usuarios: ( http://localhost:8000/api/auth/login )
authRouter.route('/login')
.post(jsonParser, async (req: Request, res: Response) => {
    
    let { email, password } = req.body
    
    if(email && password) {
      
      // Controller instance to execute method
      const controller: AuthController = new AuthController()

      const auth: IAuth = {
        email,
        password
      }
  
      // Obtain response
      const response: any = await controller.loginUser(auth)
  
      // Send to the client the response with the JsonWebToken to authorize requests
      return res.status(200).send(response)
    }
})

// Ruta protegida por VERIFY TOKEN middleware
authRouter.route('/me')
.get(verifyToken, async (req: Request, res: Response) => {

  // Obtener el id de user para ver los datos
  const id: any = req?.query?.id

  if(id){

    // Controller: AuthController
    const controller : AuthController = new AuthController()

    // Obtener respuesta de Controller
    let response: any = await controller.userData(id)

    // Si el usuario está autorizado
    return res.status(200).send(response)

  }else {
    return res.status(401).send({
      message: 'You are not authorized to perform this action'
    })
  }

})

export default authRouter