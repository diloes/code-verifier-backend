/**
 * Root Router
 * Redirections to Routers
 * ( Encargado de gestionar las redirecciones )
 */

import express, { Request, Response } from 'express'
import { LogInfo } from '../utils/logger'
import helloRouter from './HelloRouter'
import goodbyeRouter from './GoodbyeRouter'
import usersRouter from './UserRouter'
import katasRouter from './KatasRouter'
import authRouter from './AuthRouter'

// Server instance
const server = express()

// Router instance( it only handles requests to '/api ' )
const rootRouter = express.Router()

// Activate for requests to http://localhost:8000/api
rootRouter.get('/', (req: Request, res: Response) => {
  LogInfo('GET: http://localhost:8000/api')
  res.send('Welcome to APP Express + TS + Swagger + Mongoose')
})

// Redirections to Routers & Controllers
server.use('/', rootRouter) // http://localhost:8000/api -> RootRouter
server.use('/hello', helloRouter) // http://localhost:8000/api/hello -> HelloRouter
server.use('/goodbye', goodbyeRouter) // htttp:localhost:8000/api/goodbye -> GoodbyeRouter
server.use('/users', usersRouter) // http://localhost:8000/api/users -> UserRouter
server.use('/auth', authRouter) // http://localhost:8000/api/auth -> authRouter
server.use('/katas', katasRouter) // http://localhost:8000/api/katas -> katasRouter

export default server
