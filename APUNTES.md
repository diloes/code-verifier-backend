# Apuntes varios sobre el proyecto y en general

## Como funciona
 1. El entrypoint es index.ts 
 2. En /server/index.ts inicia el servidor con express y conecta moongose a la base de datos.
 3. En /routes/index.ts distribuye las rutas a los controladores.
 4. En /controllers/ tenemos los controladores de cada ruta que se encargan de las peticiones
 http (get, post, put, delete).
 5. En /domain/orm tenemos los orm que interactuan con la base de datos.
 6. En /domain/entities tenemos los archivos que seleccionan las colecciones que vamos a trabajar.

## Clases vs Interfaces vs Types
Creamos una clase cuando queremos implementar una serie de funcionalidades
cons sus constructores, métodos, etc. 

Creamos una interface cuando queremos definir la estructura del comportamiento
mínimo que tiene que cumplir algo que lo implemente. 

Creamos un type cuando no necesitamos tanta información, simplemente 
queremos crear tipos propios. Existen los tipos primitivos(string, number, boolean, etc),
los tipos complejos(array, object, etc) y los tipos complejos personalizados(custom types)
que son los que creamos nosotros.

La idea de la interface es definir el aspecto que van a tener los controllers, los types 
van a ser los tipos de datos que vamos a usar dentro de los controllers y los archivos
controllers van a ser los encargados de devolver determinada información que va ser 
gestionada a través de las rutas.  

## Instalar typescript 
Para iniciar o instalar typescript en el proyecto ponemos el siguiente comando
en consola -> npx tsc --init

## path en tsconfig.json
"paths": {
      "@/*": [
        "src/*"
      ]
-> Quiere decir que sustituimos el src por la @ (Llevamos ya 8 sesiones y esto no ha funcionado,
hemos tenido que volver a poner los dos puntos)

## ORM
Es el que interactua con la base de datos. 

## Entidad(Entity)
Es la 'tabla' que se va a gestionar en la base de datos.

## Middleware
Un middleware es una función que se ejecuta antes de que una ruta sea ejecutada.