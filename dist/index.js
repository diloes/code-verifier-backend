"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const server_1 = __importDefault(require("./src/server"));
const logger_1 = require("./src/utils/logger");
// Configuration the .env file
dotenv_1.default.config();
const port = process.env.PORT || 8000;
// Execute server
server_1.default.listen(port, () => {
    console.log(`[SERVER ON]: Running in http://localhost:${port}/api`);
});
// Control SERVER ERROR
server_1.default.on('error', error => {
    (0, logger_1.LogError)(`[SERVER ERROR]: ${error}`);
});
/*
import express, { Express, Request, Response } from 'express'
import dotenv from 'dotenv'

// Load env variables from .env file
dotenv.config()

// Create Express App
const app: Express = express()
const port: string | number = process.env.PORT || 8000

// The client makes .get request to the server and the server sends back a response to the client
app.get('/', ( req: Request, res: Response ) => {
  // Send Welcome
  res.send('Welcome to APP Express + TS + Swagger + Mongoose')
})

// EJERCICIO EXTRA: Modificar ruta hello y devolver json con nombre recibido en la ruta
app.get('/hello', ( req: Request, res: Response ) => {

  // La ruta hello recibe un parametro llamado name
  const name = req.query.name

  // Devuleve un 200 con el json
  res.json({
    data: {
      "message": `Hola ${ name || 'anonimo' }anónimo`
    }
  })
})

// EJERCICIO: Creación de ruta que devuelva una res 200 con un json
app.get('/exercise', ( req: Request, res: Response ) => {

  // Create a json object
  const json = {
    data: {
      "message": "Goodbye, world"
    }
  }

  res.send(json)
})

// Execute app and listen requests on port 8000
app.listen( port, () => {
  console.log(`EXPRESS SERVER: Running at http://localhost:${port}`)
})
*/
//# sourceMappingURL=index.js.map